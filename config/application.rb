require_relative 'boot'

require 'rails/all'

require "sprockets/railtie"
# require "rails/test_unit/railtie"
require 'sprockets/es6'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Maximus
  class Application < Rails::Application
    # Initialize configuration defaults for originally generated Rails version.
    config.load_defaults 5.2

    config.generators do |g|
      g.assets false
      g.helper false
      g.stylesheets false
      g.template_engine :slim
    end

    config.active_record.default_timezone = :local
    config.autoload_paths << Rails.root.join('lib')

    config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/
    config.assets.precompile = ["application.es6"]
    config.assets.initialize_on_precompile = false

    config.assets.paths << Rails.root.join('app', 'assets', 'fonts')
    # config.assets.precompile << /\.(?:svg|eot|woff|ttf)$/
    config.time_zone = 'Mexico City'
  end
end
